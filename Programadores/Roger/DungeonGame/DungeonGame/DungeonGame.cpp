/*
Cosas que hacer:
	-Una array de 5 cuentas ya creadas
	-Cada cuenta debe tener: #Nombre de usuarios, #Contraseņa, #Listado de personajes (empieza vacio), #Si es administrador
	-Se debe intoducir correctamente el nombre de usuario y contraseņa
	-Una vez en la cuenta debe darte las opciones de cuenta
	-Las opciones de cuenta son: #Crear personaje, #Listar los personajes, #Eliminar personages, #Salir
	-Al crear un personage te deberan darte las opciones de #Nombrar al personage, #Raza (Barbaro, Asesino, Arquero, Nigromante, Hechizero)
	-Si alcanza el maximo de personages no se podran crear mas
	-Al finalizar la creacion del personage vuelve al menu inicial
	-Una cuenta deberia ser de administrador
	-Las opciones del administrador deben ser: #Lista de cuentas, #Eliminar cuentas de jugadores, #Mostrar de los personages de una cuenta, #Eliminar personages, #Salir
*/
#include<iostream>
#include<string>

//Constantes para definir el maximo de cunetas en el juego y maximo de personages por cuenta
int const MAX_COUNT = 5;
int const MAX_CHARACTER = 8;


//Enumeracion de las deistintas clases que puede ser un personage
enum Races
{
	BARBARIAN = 1,
	ASESIN = 2,
	ARCHER = 3,
	NECROMANCER = 4,
	SORCERER = 5
};

//Estructura de un personage;
struct Character
{
	std::string charName;
	int race;
};

//Estructura de una cuenta;
struct Count
{
	std::string countName;
	std::string countPasswodr;
	bool admin;
	Character countCharacters[MAX_CHARACTER];
};

//Funcion para definir las cuentas;
//Se crea una array estatica de cuentas y se introducen en la array del programa principal;
void InitCounts(Count (&counts) [MAX_COUNT])
{
	//Cuentas definidas a mano (la cuenta enti es el administrador);
	Count definedCounts[MAX_COUNT] = { {"ol","lo",false},{"ko","ok",false},{"po","op",false},{"gf","fg",false},{"enti","enti",true} };

	for (int i = 0; i < MAX_COUNT; i++)
	{
		counts[i] = definedCounts[i];
	}
	
}

//Funcion para introducir los usuarios y contraseņas, y comprobar que exista la cuenta y si es administrador;
//Primero revisa si el nombre y la contraseņa coincide con alguna cuenta
//Si coincide, modifica la posicion de esa cuenta en la array para poderla identificar mas adelante;
//Una vez se ha identificado, la funcion nos devolvera si es administrador con un booleano;
bool LogIn(const Count counts[MAX_COUNT], int &countPos) 
{
	std::string inputUser;
	std::string inputPassword;
	bool admin = false;
	bool loged = false;

	while (true)
	{
		std::cout << "\nIntroduce el nombre de ususario:\n - ";
		std::cin >> inputUser;
		std::cout << "\nIntroduce la contrseņa:\n - ";
		std::cin >> inputPassword;

		for (int i = 0; i < MAX_COUNT; i++)
		{
			if (inputUser == counts[i].countName && inputPassword == counts[i].countPasswodr)
			{
				loged = true;
				countPos = i;

				if (counts[i].admin)
				{
					admin = true;
				}
				i = MAX_COUNT;
			}
		}

		if (loged && admin)
		{
			return true;
		}
		else if (loged)
		{
			return false;
		}
		else
		{
			std::cout << "\nEl usuario y la contrseņa no coinciden\n";
		}
	}

}

//Funcion para crear a un personage de una cuenta no admin;
//Primero pregunta que nombre le quieres dar al personage;
//Luego comprueba que ese nombre no se haya dado antes a otro personage y de ser el caso devuelve error;
//Si el nombre es posible, luego se pregunta la raza siguiendo el enum del principio;
//Si ya tienes el maximo posible de personages, te dara error;
void CreateCharacter(Count &count)
{
	bool checked = true;

	for (int i = 0; i < MAX_CHARACTER; i++)
	{
		if (count.countCharacters[i].charName == "")
		{
			do
			{
				std::cout << "\n - Que nombre quieres que tenga tu personage?\n";
				std::cin >> count.countCharacters[i].charName;

				for (int j = 0; j < MAX_CHARACTER; j++)
				{
					if (count.countCharacters[i].charName == count.countCharacters[j].charName && i != j)
					{
						checked = false;

						std::cout << "\n !! Ese nombre de personage ya existe en tu cuenta. Vuelve a introducir un nombre\n";

						j = MAX_CHARACTER;
					}
					else
					{
						checked = true;
					}
				}
			} while (!checked);

			std::cout << "\n - Que raza quieres que sea tu personage?\n 1- Barbaro ( Escribe \"1\" )\n\n 2- Asesino ( Escribe \"2\" )\n\n 3- Arquero ( Escribe \"3\" )\n\n 4- Necromantico ( Escribe \"4\" )\n\n 5- Hechizero ( Escribe \"5\" )\n ";
			std::cin >> count.countCharacters[i].race;
			i = MAX_CHARACTER;
		}
		else if (i+1 >= MAX_CHARACTER)
		{
			std::cout << "\n !! Ya has creado el maximo numero de personages posibles.\n";
		}
	}
}

//Funcion que lista todos los personages que tiene la cuenta no admin;
//Es un cout que muestra todos los personages y su raza;
void ListChar(Count &count)
{
	for (int i = 0; i < MAX_CHARACTER; i++)
	{
		if (count.countCharacters[i].charName != "")
		{
			std::cout << "\n - " << count.countCharacters[i].charName << " ";

			switch (count.countCharacters[i].race)
			{
			case 1:
				std::cout << "Barbaro\n";
				break;

			case 2:
				std::cout << "Asesino\n";
				break;

			case 3:
				std::cout << "Arquero\n";
				break;

			case 4:
				std::cout << "Necromantico\n";
				break;

			case 5:
				std::cout << "Hechizero\n";
				break;
			default:
				break;
			}
		}
	}
}

//Funcion para eliminar el personage introducido de una cuenta no admin;
//Revisa que el nombre que introducimos corresponda con el nombre de algun personage;
//Si se encuentra al personage lo que se hace es que movemos a una posicion los elemntos de la array, y el prosnage que decidimos eliminar queda sobreescrito por el resto de posiciones manteniendo el orden;
//Creamos un Character vacio y se introduce en el ultimo puesto;
void DeleteChar(Count &count)
{
	std::string deleteName;

	Character emptyChar;

	std::cout << "\n - Escribe el nombre del personage que quieres eliminar:\n";
	std::cin >> deleteName;

	for (int i = 0; i < MAX_CHARACTER; i++)
	{
		if (deleteName == count.countCharacters[i].charName)
		{
			std::cout << "\n - Se he eliminado el persoange " << count.countCharacters[i].charName << "\n";

			for (int j = i+1; j < MAX_CHARACTER; j++)
			{
				count.countCharacters[j - 1].charName = count.countCharacters[j].charName;
			}
			count.countCharacters[MAX_CHARACTER - 1] = emptyChar;
			i = MAX_CHARACTER;
		}
		else if (i+1 >= MAX_CHARACTER)
		{
			std::cout << "\n !! No se ha encontrado ningun personage con ese nombre\n";
		}
	}
}

//Funcion para eliminar una cuenta no admin;
//Se substituye la cuenta actual con una cuenta vacia;
void DeleteCount(Count &count)
{
	Count emptyCount;

	std::cout << "La cuenta de " << count.countName << " ha sido eliminada";

	count = emptyCount;
}

//Funcion para mostrar el menu del usuario no administrador;
//Crea un enum con cada opcion disponible y luego cada elemento ejecuta la funcion corresponiente;
//La funcion de DeleteChar y DeleteCount son las mismas que con la cuenta administrador pero con unos constructores diferentes;
//La opcion de DeleteCount y Exit tienen el return porque deben volver al login, el resto de opciones deben volver al menu;
int menuUsuario(Count &count)
{
	enum Options
	{
		CREATE = 1,
		LIST = 2,
		DELETE_CHAR = 3,
		DELETE_COUNT =4,
		EXIT = 5
	};

	int decision;

	while (true)
	{
		std::cout << "\nBienvenido " << count.countName << " que quieres hacer?\n\n 1- Crear un personage ( Escribe \"1\" )\n\n 2- Listar los persoanges ( Escribe \"2\" )\n\n 3- Eliminar algun persoange ( Escribe \"3\" )\n\n 4- Eliminar la cuenta ( Escribe \"4\" )\n\n 5 - Salir(Escribe \"5\" )\n";
		std::cin >> decision;

		switch (decision)
		{
		case Options::CREATE:
			CreateCharacter(count);
			break;
		case Options::LIST:
			ListChar(count);
			break;
		case Options::DELETE_CHAR:
			DeleteChar(count);
			break;
		case Options::DELETE_COUNT:
			DeleteCount(count);
			return 0;
			break;
		case Options::EXIT:
			return 0;
			break;
		default:
			break;
		}
	}
}

//Funcion para mostrar el resto cunetas que no sean administrador;
//Primero revisa todas la cuentas con nombre y cuenta el numero de personages de cada una;
//Hace un cout con el nombre de cuentas y el numero de personages que se ha contado;
void ListCounts(Count(&counts)[MAX_COUNT])
{
	for (int i = 0; i < MAX_COUNT; i++)
	{
		int	charCount = 0;

		for (int j = 0; j < MAX_CHARACTER; j++)
		{
			if (counts[i].countCharacters[j].charName != "")
			{
				charCount++;
			}
		}

		if (counts[i].countName != "" || !counts[i].admin)
		{
			std::cout << "\n" << counts[i].countName << " : " << charCount << " personages creados\n";
		}
	}
}


//Funcion para eliminar alguna cuenta no administrador;
//Comprueba que el nombre que se introduze coincide con el de alguna cuenta;
//Una vez se detecta la cuenta se mueve todas las siguientes cuentas a una posicion anterior, quedando la cuenta substituida por las siguientes manteniendo el orden;
//Se pone una cuenta vacia en la ultima posicion;
void DeleteCount(Count(&counts)[MAX_COUNT])
{
	std::string deleteName;
	bool founded = false;

	Count emptyCount;

	std::cout << "\nEscribe el nomre de la cuenta que quieras eliminar :\n";
	std::cin >> deleteName;

	for (int i = 0; i < MAX_COUNT; i++)
	{
		if (counts[i].countName == deleteName && !counts[i].admin)
		{
			std::cout << "La cuenta de " << counts[i].countName << " ha sido eliminada\n";

			for (int j = i+1; j < MAX_COUNT; j++)
			{
				counts[j - 1] = counts[j];
			}

			counts[MAX_COUNT - 1] = emptyCount;

			i = MAX_COUNT;
		}
		else if (i >= MAX_COUNT)
		{
			std::cout << "\n!! No se ha encontrado la cuneta";
		}
	}
}

//Funcion que lista todos los personages de una cuenta no administrador concreta;
//Se comprueba que el nombre introducido coincida con alguna cuenta;
//Si coincide se listan todos los personages con sus clases;
//Si no se encuentra la cuenta sale error;
void ShowChar(Count(&counts)[MAX_COUNT])
{
	std::string showCount;

	std::cout << "\nEscribe el nomre de la cuenta que quieres quieres ver :\n";
	std::cin >> showCount;

	for (int i = 0; i < MAX_COUNT; i++)
	{
		if (counts[i].countName == showCount)
		{
			for (int j = 0; j < MAX_CHARACTER; j++)
			{
				if (counts[i].countCharacters[j].charName != "")
				{
					std::cout << "\n - " << counts[i].countCharacters[j].charName;

					switch (counts[i].countCharacters[j].race)
					{
					case 1:
						std::cout << " Barbaro\n";
						break;

					case 2:
						std::cout << " Asesino\n";
						break;

					case 3:
						std::cout << " Arquero\n";
						break;

					case 4:
						std::cout << " Necromantico\n";
						break;

					case 5:
						std::cout << " Hechizero\n";
						break;
					default:
						break;
					}
				}
			}

			i = MAX_COUNT;
		}
		else if (i >= MAX_COUNT)
		{
			std::cout << "\n!! No se ha encontrado la cuneta";
		}
	}
}

//Funcion para eliminar un personage concreto de una cuenta no administrador;
//Pide que se introduzca primero el nombre de la cuenta y luego el nombre del personage;
//Si todo coincide se substituye al personage con los personages hay a continuacion en la array;
//Creamos un personage vacio y se coloca al final de la array;
void DeleteChar(Count(&counts)[MAX_COUNT])
{
	std::string deleteCount;
	std::string deleteChar;

	std::cout << "\nEscribe la cuenta de la quieres borrar el personage:\n";
	std::cin >> deleteCount;

	Character emptyChar;

	for (int i = 0; i < MAX_COUNT; i++)
	{
		if (counts[i].countName == deleteCount)
		{
			std::cout << "\nAhora escribe el nombre del perosnage que quieres eliminar:\n";
			std::cin >> deleteChar;

			for (int j = 0; j < MAX_CHARACTER; j++)
			{
				if (counts[i].countCharacters[j].charName == deleteChar)
				{
					std::cout << "\nEl personage " << counts[i].countCharacters[j].charName << " ha sido eliminado";

					for (int z = j+1; z < MAX_CHARACTER; z++)
					{
						counts[i].countCharacters[z - 1] = counts[i].countCharacters[z];
					}

					counts[i].countCharacters[MAX_CHARACTER] = emptyChar;

					j = MAX_CHARACTER;
				}
				else if (j >= MAX_CHARACTER)
				{
					std::cout << "\n!! No se ha encontrado el personage";
				}
			}

			i = MAX_COUNT;
		}
		else if (i >= MAX_COUNT)
		{
			std::cout << "\n!! No se ha encontrado la cuneta";
		}
	}
}

//Funcion del Menu de adminstrador;
//Todas la opciones se definen en un enum, y luego se ejecuta la funcion adequada segun la opcion escogida;
int menuAdmin(Count (&counts)[MAX_COUNT],int actualCount)
{
	enum Options
	{
		LIST = 1,
		DELETE = 2,
		SHOW_CHAR = 3,
		DELETE_CHAR = 4,
		EXIT = 5,
	};

	int decision;

	while (true)
	{
		std::cout << "\nBienvenido " << counts[actualCount].countName << " que quieres hacer?\n\n 1- Listar al resto de cuentas ( Escribe \"1\" )\n\n 2- Eliminar una cuenta ( Escribe \"2\" )\n\n 3- Mostrar los personages de una cuenta ( Escribe \"3\" )\n\n 4- Eliminar a un personage de una cuenta ( Escribe \"4\" )\n\n 4- Salir ( Escribe \"5\" )\n";
		std::cin >> decision;

		switch (decision)
		{
		case 1:
			ListCounts(counts);
			break;
		case 2:
			DeleteCount(counts);
			break;
		case 3:
			ShowChar(counts);
			break;
		case 4:
			DeleteChar(counts);
			break;
		case 5:
			return 0;
		default:
			break;
		}
	}
}

//Programa principal;
void main()
{
	Count counts[MAX_COUNT];
	int countPos;

	InitCounts(counts);

	while (true)
	{
		//La funcion LogIn debuelve si el usuario es administrador o no;
		if (LogIn(counts, countPos))
		{
			menuAdmin(counts, countPos);
		}
		else
		{
			menuUsuario(counts[countPos]);
		}
	}

	std::cin.ignore();
	std::cin.get();
}