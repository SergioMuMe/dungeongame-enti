#include <iostream>
#include <windows.h>

/*
Code author : Duthomhas
void ClearScreen() : http://www.cplusplus.com/articles/4z18T05o/#OSSpecificWays
Why system() is evil : http://www.cplusplus.com/articles/j3wTURfi/
*/

void ClearScreen()
{
	HANDLE                     hStdOut;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	DWORD                      count;
	DWORD                      cellCount;
	COORD                      homeCoords = { 0, 0 };

	hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	if (hStdOut == INVALID_HANDLE_VALUE) return;

	/* Get the number of cells in the current buffer */
	if (!GetConsoleScreenBufferInfo(hStdOut, &csbi)) return;
	cellCount = csbi.dwSize.X *csbi.dwSize.Y;

	/* Fill the entire buffer with spaces */
	if (!FillConsoleOutputCharacter(
		hStdOut,
		(TCHAR) ' ',
		cellCount,
		homeCoords,
		&count
	)) return;

	/* Fill the entire buffer with the current colors and attributes */
	if (!FillConsoleOutputAttribute(
		hStdOut,
		csbi.wAttributes,
		cellCount,
		homeCoords,
		&count
	)) return;

	/* Move the cursor home */
	SetConsoleCursorPosition(hStdOut, homeCoords);
}

// Muestra mensaje "Pulsa cualquier tecla para continuar..." a continuación hace un ClearScreen()
void PulsaContinuar()
{
	std::cout << "\nPulsa cualquier tecla para continuar...";
	std::cin.ignore();
	std::cin.get();
	ClearScreen();
}