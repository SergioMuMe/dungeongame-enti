#include <string>
#include <array>
#include <iostream>
#include <wchar.h> // to include accent marks
#include <locale.h> // to include accent marks
#include <windows.h>

#define MAXCHARACTERS 8
#define STARTINGPLAYERS 4 
#define STARTINGACCOUNTS 5

struct Characters {
	unsigned int charClass;
	std::string charName;
};

struct Players {
	std::string name;
	std::string password;
	int indexChar = 0;
	Characters aCharacters[MAXCHARACTERS];
	bool admin;
};

Players aPlayer[STARTINGACCOUNTS];
int currentPlayer = 0;
enum CLASS { B�rbaro, Asesino, Arquero, Nigromante, Hechizero };


void CreateChar(Players* aPlayer, int currentPlayer, Characters* &characters) {

	int index = aPlayer[currentPlayer].indexChar;
	std::string newName;

	std::cout << "Introduce nombre para el nuevo personaje (posicion " << index; << "): ";
	std::cin >> newName;
	aPlayer[currentPlayer].aCharacters[index].charName = newName;

}


int main() {
	setlocale(LC_ALL, "");

	InitAccounts(aPlayer);
	Login(aPlayer, currentPlayer);
	ChargeMenu(aPlayer, currentPlayer);

}