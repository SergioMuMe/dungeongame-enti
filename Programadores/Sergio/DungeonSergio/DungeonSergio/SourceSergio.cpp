#include <iostream>
#include <wchar.h> // to include accent marks
#include <locale.h> // to include accent marks
#include <array>
#include <string>

//Header File with custom functions created from us or taken from internet
#include "referencias.h"

//DEFINITIONS
#define MAX_PLAYERS 5 // Original 5
#define MAX_CHARACTERS 8 // Original 8

//Technical specifications: https://docs.google.com/document/d/1aYVnKBsf0jHhKYCY1bemWhuxkmDH66bi5xPk0OzjRYY/edit#heading=h.pdi40vkb9wzw
//All design decisions, technical guides and testing process are defined in this document.

enum class ROLE { 
	EMPTY,
	ADMIN,
	USER,
	COUNT
};

// R�BRICA 3.a
enum class RACE {
	EMPTY,
	BARBARO,
	ASESINO,
	ARQUERO,
	NIGROMANTE,
	HECHIZERO,
	COUNT
};

//Display an ordered list of each Race
void DisplayRaces()
{
	std::cout <<
		"1 - Barbaro\n" <<
		"2 - Asesino\n" <<
		"3 - Arquero\n" <<
		"4 - Nigromante\n" <<
		"5 - Hechicero\n";
}


struct PLAYER {
	std::string name;
	std::string password;
	int id;
	int characterIndex=0; //Marks the array position where the new character must be created.
	int charactersCreated = 0; //Validates the number of characters created.
	std::array <std::string, MAX_CHARACTERS> characterName;
	std::array <RACE, MAX_CHARACTERS> characterRace;
	ROLE userRole; 
	
};

//Display a specific race of a one user character
std::string DisplayRace(std::array <PLAYER, MAX_PLAYERS> playerList, int target, int i)
{
	switch (playerList[target].characterRace[i])
	{
	case (RACE::BARBARO):
		return "Barbaro";
		break;
	case (RACE::ASESINO):
		return "Asesino";
		break;
	case (RACE::ARQUERO):
		return "Arquero";
		break;
	case (RACE::NIGROMANTE):
		return "Nigromante";
		break;
	case (RACE::HECHIZERO):
		return "Hechicero";
		break;
	}
	return "ERROR";
}

bool LoginValidation(std::array <PLAYER, MAX_PLAYERS> playerList, std::string user, std::string pass, int &logged)
{
	for (int i = 0; i < MAX_PLAYERS; i++)
	{
		if (playerList[i].name == user)
		{
			if (playerList[i].password == pass)
			{
				std::cout << "\nLOGIN OK" << std::endl;
				logged = i;
				PulsaContinuar();
				return false; // Match for user and pass
			}
		}
	}

	//R�BRICA 1.b
	std::cout << "\nUsuario y/o contrase�a incorrecta. Vuelve a intentarlo." << std::endl;
	PulsaContinuar();

	return true; //No match for user and/or pass
}


//Set name&race to character. Validations: No size=0 name, unique name. 
void ValidateCharacter(std::array <PLAYER, MAX_PLAYERS> &playerList, int target)
{

	bool error=false;
	bool exit = false;
	int newCharacterRace;
	std::string newCharacterName;
	
	

	do
	{
		ClearScreen();
		std::cout << "\nOpcion 1 - Crear personaje" << std::endl;
		std::cout << "Nombre del personaje: ";
		std::cin >> newCharacterName;
		error = false;

		if (newCharacterName.size() == 0) // Validation: Name can't be size 0.
		{
			std::cout << "\nERROR: El nombre no puede estar vacio." << std::endl;
			PulsaContinuar();
		}
		else // R�BRICA 3.c Validation: Name already used, must be unique. 
		{
			for (int i = 0; i < MAX_PLAYERS; i++)
			{
				for (int j = 0; j < MAX_CHARACTERS; j++)
				{
					if (newCharacterName == playerList[i].characterName[j])
					{
						std::cout << "\nERROR: El nombre \"" << newCharacterName << "\" ya existe. Por favor, introduce un nuevo nombre." << std::endl;
						PulsaContinuar();
						error = true;
						break;
					}
				}
				if (error) { break; }
			}
		}

		if (!error)
		{
			std::cout << "\n\nRazas disponibles:\n";
			DisplayRaces();
			do
			{
				std::cout << "Raza del personaje (especifica el n�mero): ";
				std::cin >> newCharacterRace;
			} while (newCharacterRace <= 0 || newCharacterRace >= (int)RACE::COUNT);
		}
		


		if (!error) //Name and race OK, assign new character
		{
			for (int i = 0; i < MAX_CHARACTERS; i++) //Stablish the first empty position in array to be the newCharacter. R�BRICA 3.b
			{
				if (playerList[target].characterName[i].empty())
				{
					playerList[target].characterIndex = i;
					break;
				}
			}
			playerList[target].characterName[playerList[target].characterIndex] = newCharacterName;
			playerList[target].characterRace[playerList[target].characterIndex] = (RACE)newCharacterRace;
			playerList[target].charactersCreated++;
			exit = true;
		}
	
	} while (!exit);
	
	PulsaContinuar();
}

void ListarPersonajes(std::array <PLAYER, MAX_PLAYERS> &playerList, int target)
{
	int aux = 1;
	if (playerList[target].charactersCreated != 0)
	{
		std::cout << "\nListado de personajes de la cuenta " << playerList[target].name << ":" << std::endl;
		for (int i = 0; i < MAX_CHARACTERS; i++)
		{
			if (!playerList[target].characterName[i].empty()) //R�BRICA 4.b
			{
				std::cout << "\nPERSONAJE " << aux++ << std::endl << //R�BRICA 4.a
					"Nombre: " << playerList[target].characterName[i] << std::endl <<
					"Raza: " << DisplayRace(playerList, target, i) << std::endl <<
					"-----------------\n\n";
			}
		}
	}
	else
	{
		std::cout << "El usuario " << playerList[target].name << " no dispone de personajes creados." << std::endl;
	}
}

std::string ValidateDeleteAccount(std::array <PLAYER, MAX_PLAYERS> &playerList, int target) {
	
	std::string nombre;

	std::cout << "\n�Estas seguro de que quieres eliminar esta cuenta? Escribe el nombre de usuario (" << playerList[target].name << ") para confirmar." << std::endl;
	std::cin >> nombre;

	return nombre;
}

std::string ValidateDeleteAccount(std::array <PLAYER, MAX_PLAYERS> &playerList, std::string target) {

	std::string nombre;

	std::cout << "\n�Estas seguro de que quieres eliminar esa cuenta de usuario? Escribe de nuevo el nombre de usuario (" << target << ") para confirmar." << std::endl;
	std::cin >> nombre;

	return nombre;
}

int EliminarPersonaje(std::array <PLAYER, MAX_PLAYERS> &playerList, int target) 
{
	std::string nombre, aux;

	ListarPersonajes(playerList, target);

	std::cout << "\nEscribe el nombre del personaje que deseas eliminar: ";
	std::cin >> nombre;

	for (int i = 0; i < MAX_CHARACTERS; i++)
	{
		if (playerList[target].characterName[i] == nombre) //R�BRICA 5.a
		{
			std::cout << "Se ha encontrado el personaje \"" << nombre << "\"." << std::endl; //R�BRICA 5.b
			std::cout << "Para confirmar, escribe la palabra en mayusculas ELIMINAR:";
			std::cin >> aux;
			if (aux == "ELIMINAR")
			{
				playerList[target].characterName[i].clear();
				playerList[target].characterRace[i] = RACE::EMPTY;
				playerList[target].charactersCreated--;
				std::cout << "\nPersonaje eliminado.";
				PulsaContinuar();
				return 0;
			}
			std::cout << "\nBorrado de personaje cancelado. Volviendo al menu." << std::endl;
			PulsaContinuar();
			return 0;
		}
	}
	std::cout << "No existe ningun personaje con el nombre \""<< nombre << "\"." << std::endl; //R�BRICA 5.c
	PulsaContinuar();
}

void DisplayPlayerList(std::array <PLAYER, MAX_PLAYERS> &playerList)
{
	std::cout << "\nListado de personajes:" << std::endl;
	for (int i = 0; i < MAX_PLAYERS; i++)
	{
		if (!playerList[i].name.empty() && playerList[i].userRole==ROLE::USER) //R�BRICA 7.c 7.a
		{
			std::cout << "\nUsuario " << playerList[i].name << "(Personajes:" << playerList[i].charactersCreated << ")." << std::endl; //R�BRICA 7.b
		}
	}	
}

bool LoadGameMenu(std::string menu, std::array <PLAYER, MAX_PLAYERS> &playerList, int logged)
{
	int opcion;
	bool exitMenu = true;
	std::string target;
	int aux;
	bool found=false;

	// #### MENU ADMIN ####

	
	if (menu == "Admin")
	{
		do
		{
			// ::: Display options :::
			std::cout	<< "1. Listar cuentas de jugador" << std::endl
						<< "2. Eliminar cuenta de jugador" << std::endl
						<< "3. Mostrar personajes de un jugador" << std::endl
						<< "4. Eliminar personajes de un jugador" << std::endl
						<< "5. Salir" << std::endl;

			std::cout << "Opci�n: " << std::endl;
			std::cin >> opcion;

			// ::: Admin options :::

			switch (opcion)
			{
			case(1): // Opcion Listar cuentas de jugador
				std::cout << "\n Opcion 1" << std::endl;
				DisplayPlayerList(playerList);
				PulsaContinuar();
				break;
			case(2): // Opcion Eliminar cuenta de jugador
				std::cout << "\n Opcion 2" << std::endl;
				DisplayPlayerList(playerList);
				std::cout << "Escribe el nombre de la cuenta a eliminar:";
				std::cin >> target;

				for (aux = 0; aux < MAX_PLAYERS; aux++)
				{
					if (playerList[aux].name==target)
					{
						found = true;
						break;
					}
				}

				if (found)
				{
					if (playerList[aux].userRole == ROLE::ADMIN)
					{
						std::cout << "No se puede eliminar una cuenta administrador."; //R�BRICA 8.c
						PulsaContinuar();
					}
					else if (ValidateDeleteAccount(playerList, target) == target) //R�BRICA 8.a
					{
						std::cout << "Procediendo a eliminar el jugador no administrador " << playerList[aux].name; //R�BRICA 8.b
						playerList[aux].name.clear();
						playerList[aux].password.clear();
						for (int i = 0; i < MAX_CHARACTERS; i++)
						{
							playerList[aux].characterName[i].clear();
							playerList[aux].characterRace[i] = RACE::COUNT;
							playerList[aux].charactersCreated = 0;
						}
						PulsaContinuar();
					}
					else
					{
						std::cout << "\n\nNombre incorrecto. Desconectando..." << std::endl; 
						PulsaContinuar();
					}
				}
				else
				{
					std::cout << "\n\nNombre no encontrado. Desconectando..." << std::endl; //R�BRICA 8.c
					PulsaContinuar();
				}
	
				found = false;
				break;
			case(3): // Opcion Mostrar personajes de un jugador
				std::cout << "\n Opcion 3" << std::endl;
				
				DisplayPlayerList(playerList);
				std::cout << "Escribe el nombre de la cuenta:";
				std::cin >> target;

				for (aux = 0; aux < MAX_PLAYERS; aux++)
				{
					if (playerList[aux].name == target)
					{
						found = true;
						break;
					}
				}
				ListarPersonajes(playerList, aux);
				found = false;
				PulsaContinuar();

				break;
			case(4): // Opcion Eliminar personaje de jugador
				std::cout << "\n Opcion 4" << std::endl;
				DisplayPlayerList(playerList);
				std::cout << "Escribe el nombre de la cuenta:"; //esto podria ir en una funcion !!!!
				std::cin >> target;
				for (aux = 0; aux < MAX_PLAYERS; aux++)
				{
					if (playerList[aux].name == target)
					{
						found = true;
						break;
					}
				}
				EliminarPersonaje(playerList, aux);
				break;
			case(5): // Opcion Salir
				exitMenu = false; //R�BRICA 2.b
				break;
			default:
				std::cout << "\nNo he entendido tu opci�n.";
				PulsaContinuar();
				break;
			}

		} while (exitMenu); //R�BRICA 2.a

		return true; //Vuelve al menu LOGIN
	}
	

	// #### MENU USER ####

	// ::: Variables User :::

	std::string newCharacterName;
	ROLE newCharacterRace;

	// ::: Menu :::

	if (menu == "User")
	{
		do
		{
			// ::: Display options :::
			std::cout	<< "\n1. Crear personaje" << std::endl
						<< "2. Listar personajes" << std::endl
						<< "3. Eliminar personaje" << std::endl
						<< "4. Eliminar cuenta" << std::endl
						<< "5. Salir" << std::endl;

			std::cout << "Opci�n: " << std::endl;
			std::cin >> opcion;

			// ::: User options :::

			switch (opcion)
			{
			case(1): // Opcion Crear personaje
				
				if ((playerList[logged].charactersCreated) >= MAX_CHARACTERS) // R�BRICA 3.c Validation: Max characters 
				{
					std::cout << "Se ha alcanzado el n�mero m�ximo de personajes en esta cuenta." << std::endl;
					PulsaContinuar();
				}
				else // Create character
				{
					ValidateCharacter(playerList, logged);
				}
				break;
			case(2): // Opcion Listar personajes
				ClearScreen();
				std::cout << "\n Opcion 2" << std::endl;
				ListarPersonajes(playerList, logged);
				
				break;
			case(3): // Opcion Eliminar personaje
				ClearScreen();
				std::cout << "\n Opcion 3" << std::endl;
				EliminarPersonaje(playerList, logged);
				break;
			case(4): // Opcion Eliminar cuenta
				ClearScreen();
				
				if (ValidateDeleteAccount(playerList, logged) == playerList[logged].name)
				{
					//R�BRICA 6.a
					playerList[logged].name.clear();
					playerList[logged].password.clear();
					for (int i = 0; i < MAX_CHARACTERS; i++)
					{
						playerList[logged].characterName[i].clear();
						playerList[logged].characterRace[i] = RACE::COUNT;
						playerList[logged].charactersCreated = 0;
					}
				}
				else
				{
					std::cout << "\n\nNombre incorrecto. Desconectando..." << std::endl;
					PulsaContinuar();
				}

				exitMenu = false; //R�BRICA 6.b
				break;
			case(5): // Opcion Salir
				exitMenu = false; //R�BRICA 2.b
				break;
			default:
				std::cout << "\nNo he entendido tu opci�n.";
				PulsaContinuar();
				break;
			}

		} while (exitMenu); //R�BRICA 2.a
		
		return true; //Vuelve al menu LOGIN
	}
	
	
	return true; //Vuelve al menu LOGIN
}

int main()
{
	
	setlocale(LC_ALL, ""); //Include char with accent mark
	
	// #### VARIABLE DECLARATION ####

	// ::: Global :::
	bool gameLoop = true;

	// ::: Users :::
	std::array <PLAYER, MAX_PLAYERS> playerList;

	// .. Admin ..
	playerList[0].name = "ENTI";
	playerList[0].password = "ENTI";
	playerList[0].userRole = ROLE::ADMIN;
	
	//TESTING: En caso de que admin necesite personajes
	/*for (int j = 0; j < MAX_CHARACTERS; j++)
	{
		playerList[0].characterName[j] = "";
		playerList[0].characterRace[j]= RACE::EMPTY;
	}*/

	
	// .. Usuarios. By default, all ..
	for (int i = 0; i < MAX_PLAYERS; i++)
	{
		if (playerList[i].userRole != ROLE::ADMIN)
		{
			playerList[i].name = "Usuario" + std::to_string(i);
			playerList[i].password = "pass" + std::to_string(i);
			playerList[i].userRole = ROLE::USER;
			playerList[i].id = i;

			for (int j = 0; j < MAX_CHARACTERS; j++)
			{
				playerList[i].characterName[j]="";
				playerList[i].characterRace[j]=RACE::EMPTY;
			}
		}		
	}

	/*TESTING ZONE*/
	playerList[1].characterName[0] = "Jose";
	playerList[1].characterRace[0] = RACE::ARQUERO;
	playerList[1].characterName[2] = "Paca";
	playerList[1].characterRace[2] = RACE::BARBARO;
	playerList[1].charactersCreated = 2;
	playerList[2].characterName[2] = "PacaS";
	playerList[2].characterRace[2] = RACE::BARBARO;
	playerList[2].charactersCreated = 1;
	playerList[3].userRole = ROLE::ADMIN;
	
	/*END TESTING ZONE*/
	
	// ::: Login variables :::
	std::string user, pass;
	int logged; // Variable to preserve the playerList[logged] into the game
	bool exitLoop = true;
	
	// ::: Menu variables :::
	std::string menu;

	// #### LOGIN ####

	do
	{
		do
		{
			ClearScreen();
			std::cout << "Username: ";
			std::cin >> user;
			std::cout << "Password: ";
			std::cin >> pass;

			exitLoop = LoginValidation(playerList, user, pass, logged);

		} while (exitLoop); //R�BRICA 1.c

		//R�BRICA 1.a
		ClearScreen();
		std::cout << "Bienvenido usuario " << playerList[logged].name << ".\n\n";
		PulsaContinuar();

		// ::: LOAD MENU :::

		//R�BRICA 1.d
		switch (playerList[logged].userRole)
		{
		case(ROLE::ADMIN):
			menu = "Admin";
			break;
		case(ROLE::USER):
			menu = "User";
			break;
		}

		gameLoop = LoadGameMenu(menu, playerList, logged);

	} while (gameLoop);
	
	std::cout << "Has salido, adios." << std::endl;
	PulsaContinuar();

	return 0;
}