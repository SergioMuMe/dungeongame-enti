#include <string>
#include <array>


#define MAXCHARACTERS 8
#define STARTINGPLAYERS 4 
#define STARTINGACCOUNTS 5

enum CHARCLASS { Barbaro = 1, Asesino, Arquero, Nigromante, Hechicero };

struct Characters {
	std::string charName;
	CHARCLASS charClass;
};

struct Players {
	std::string name;
	std::string password;
	Characters aCharacters[MAXCHARACTERS];
	bool admin;
	int indexChar = 0;
}; 

Players aPlayer[STARTINGACCOUNTS];
int currentPlayer = 0;
unsigned int currentAccounts = STARTINGACCOUNTS;
bool gameLoop = true;