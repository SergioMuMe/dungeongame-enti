#include <iostream>
#include <wchar.h> // to include accent marks
#include <locale.h> // to include accent marks
#include <windows.h>
#include "DefinitionsDungeon.h"
#include "ASCII_Art.h"

/*
GRUPO
MARC CAYMEL
ROGER BUJAN
SERGIO MURILLO
*/

void IntroContinue() {
	std::cout << "\nIntro para continuar." << std::endl;
	std::cin.ignore();
	std::cin.get();
}
void ClearScreen()
{
	HANDLE                     hStdOut;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	DWORD                      count;
	DWORD                      cellCount;
	COORD                      homeCoords = { 0, 0 };

	hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	if (hStdOut == INVALID_HANDLE_VALUE) return;

	/* Get the number of cells in the current buffer */
	if (!GetConsoleScreenBufferInfo(hStdOut, &csbi)) return;
	cellCount = csbi.dwSize.X *csbi.dwSize.Y;

	/* Fill the entire buffer with spaces */
	if (!FillConsoleOutputCharacter(
		hStdOut,
		(TCHAR) ' ',
		cellCount,
		homeCoords,
		&count
	)) return;

	/* Fill the entire buffer with the current colors and attributes */
	if (!FillConsoleOutputAttribute(
		hStdOut,
		csbi.wAttributes,
		cellCount,
		homeCoords,
		&count
	)) return;

	/* Move the cursor home */
	SetConsoleCursorPosition(hStdOut, homeCoords);
}


bool CheckLogin(Players* aPlayer, std::string &userName, std::string &userPass, int &currentPlayer) {

	for (unsigned int i = 0; i < STARTINGACCOUNTS; i++) {
		if (aPlayer[i].name == userName && aPlayer[i].password == userPass) {
			currentPlayer = i;
			return false;
		}
	}
	//1.B
	ClearScreen();
	std::cout << "Ha intoducido un nombre y/o contrase�a err�neos" << std::endl;
	//1.C
	std::cout << "Introduzca su nombre de usuario: " << std::endl;
	std::cin >> userName;
	std::cout << "\nIntroduzca su contrase�a: " << std::endl;
	std::cin >> userPass;
	return true;
}

void Login(Players* aPlayer, int &currentPlayer) {
	std::string userName;
	std::string userPass;

	std::cout << "Introduzca su nombre de usuario: " << std::endl;
	std::cin >> userName;
	std::cout << "\nIntroduzca su contrase�a: " << std::endl;
	std::cin >> userPass;

	while (CheckLogin(aPlayer, userName, userPass, currentPlayer)) {
		CheckLogin(aPlayer, userName, userPass, currentPlayer);
	}
	//1.A
	ClearScreen();
	WelcomeMessage(userName); // Nuevo Mensaje
	//std::cout << "Bienvenid@ " << userName << std::endl; // Mensaje antiguo
	IntroContinue();
}

void CreateChar(Players* &aPlayer, int currentPlayer) {
	int index = aPlayer[currentPlayer].indexChar;
	std::string newName;
	int chosenClass;
	bool checkName = false;
	int count;
	//3.D
	if (index > MAXCHARACTERS) {
		std::cout << "Ya tienes el m�ximo de personajes creados.\nIntro para volver al men�." << std::endl;
		std::cin.ignore();
		std::cin.get();
		return;
	}
	std::cout << "Introduce nombre para el nuevo personaje: " << std::endl;
	std::cin >> newName;
	//3.B
	while (!checkName) {
		for (unsigned int i = 0; i < STARTINGACCOUNTS; i++) {
			for (unsigned int j = 0; j < MAXCHARACTERS; j++) {
				if (newName == aPlayer[i].aCharacters[j].charName) {
					std::cout << "Ese nombre ya existe, elige otro nombre: " << std::endl;
					std::cin >> newName;
				}
			}
		}
		count = 0;
		for (unsigned int i = 0; i < STARTINGACCOUNTS; i++) {
			for (unsigned int j = 0; j < MAXCHARACTERS; j++) {
				if (newName != aPlayer[i].aCharacters[j].charName) {
					count++;
				}
			}
		}
		if (count == STARTINGACCOUNTS * MAXCHARACTERS) {
			checkName = true;
		}
	}
	//3.A
	aPlayer[currentPlayer].aCharacters[index].charName = newName;
	std::cout << "Elije una clase para el nuevo personaje: (1)B�rbaro, (2)Asesino, (3)Arquero, (4)Nigromante, (5)Hechizero" << std::endl;
	std::cin >> chosenClass;
	
	while (chosenClass < 1 || chosenClass > 5) {
		ClearScreen();
		std::cout << "Has introducido un valor err�neo.\nElije una clase para el nuevo personaje: (1)B�rbaro, (2)Asesino, (3)Arquero, (4)Nigromante, (5)Hechizero " << std::endl;
		std::cin >> chosenClass;
	}
	switch (chosenClass)
	{
	case Barbaro:
		aPlayer[currentPlayer].aCharacters[index].charClass = CHARCLASS::Barbaro;
		break;
	case Asesino:
		aPlayer[currentPlayer].aCharacters[index].charClass = CHARCLASS::Asesino;
		break;
	case Arquero:
		aPlayer[currentPlayer].aCharacters[index].charClass = CHARCLASS::Arquero;
		break;
	case Nigromante:
		aPlayer[currentPlayer].aCharacters[index].charClass = CHARCLASS::Nigromante;
		break;
	case Hechicero:
		aPlayer[currentPlayer].aCharacters[index].charClass = CHARCLASS::Hechicero;
		break;
	}	
	aPlayer[currentPlayer].indexChar++;
}

void ShowClass(Players* aPlayer, int currentPlayer, unsigned int i, std::string &showClass) {

	switch (aPlayer[currentPlayer].aCharacters[i].charClass)
	{
	case CHARCLASS::Barbaro:
		showClass = "B�rbaro";
		break;
	case CHARCLASS::Asesino:
		showClass = "Asesino";
		break;
	case CHARCLASS::Arquero:
		showClass = "Arquero";
		break;
	case CHARCLASS::Nigromante:
		showClass = "Nigromante";
		break;
	case CHARCLASS::Hechicero:
		showClass = "Hechicero";
		break;
	}
}

void AdminShowClass(Players* aPlayer, int playerSelected, unsigned int i, std::string &showClass) {

	switch (aPlayer[playerSelected].aCharacters[i].charClass)
	{
	case CHARCLASS::Barbaro:
		showClass = "B�rbaro";
		break;
	case CHARCLASS::Asesino:
		showClass = "Asesino";
		break;
	case CHARCLASS::Arquero:
		showClass = "Arquero";
		break;
	case CHARCLASS::Nigromante:
		showClass = "Nigromante";
		break;
	case CHARCLASS::Hechicero:
		showClass = "Hechicero";
		break;
	}
}
//4.A, 4.B
void ShowChar(Players* aPlayer, int currentPlayer) {

	std::string showClass = "";
	unsigned int index = aPlayer[currentPlayer].indexChar;

	std::cout << "Tus personajes actuales s�n: " << std::endl;
	for (unsigned int i = 0; i < index; i++) {
		ShowClass(aPlayer, currentPlayer, i, showClass);
		std::cout << aPlayer[currentPlayer].aCharacters[i].charName << ", " << showClass << "." << std::endl;
	}
	IntroContinue();
}

void DeleteChar(Players* aPlayer, int currentPlayer) {
	std::string deleteName = "";
	std::cout << "Introduce el nombre del personaje que quieras eliminar: " << std::endl;
	std::cin >> deleteName;
	unsigned int checkName;
	unsigned int index = aPlayer[currentPlayer].indexChar;
	char ans;
	checkName = 0;

	for (unsigned int i = 0; i < index; i++) {
		if (deleteName != aPlayer[currentPlayer].aCharacters[i].charName) {
			checkName++;
		}
	}
	if (checkName == index) {
		std::cout << "El personaje elegido no existe." << std::endl;
		IntroContinue();
		return;
	}
	for (unsigned int i = 0; i < index; i++) {
		if (deleteName == aPlayer[currentPlayer].aCharacters[i].charName) {
			std::cout << "�Esta seguro de querer eliminar a " << aPlayer[currentPlayer].aCharacters[i].charName << "? (S)Si (N)No" << std::endl;
			std::cin >> ans;
			while (ans != 'S' && ans != 'N' && ans != 's' && ans != 'n') {
				std::cout << "\nHas introducido una respuesta err�nea.\n�Esta seguro de querer eliminar a " << aPlayer[currentPlayer].aCharacters[i].charName << " ? (S)Si(N)No" << std::endl;;
				std::cin >> ans;
			}
			if (ans == 'S' || ans == 's') {
				for (unsigned int j = i; j < index; j++) {
					aPlayer[currentPlayer].aCharacters[j].charName = aPlayer[currentPlayer].aCharacters[j + 1].charName;
					aPlayer[currentPlayer].aCharacters[j].charClass = aPlayer[currentPlayer].aCharacters[j + 1].charClass;
				}
				aPlayer[currentPlayer].indexChar--;
			}
		}
	}
}

void DeleteSelf(Players* &aPlayer, int currentPlayer, unsigned int &currentAccounts) {
	char ans;
	std::cout << "�Esta seguro de querer eliminar su cuenta? (S)Si (N)No" << std::endl;
	std::cin >> ans;
	while (ans != 'S' && ans != 'N' && ans != 's' && ans != 'n') {
		std::cout << "\nHas introducido una respuesta err�nea.\n�Esta seguro de querer eliminar su cuenta? (S)Si (N)No?" << std::endl;
		std::cin >> ans;
	}
	if (ans == 'S' || ans == 's') {
		for ( int i = currentPlayer; i < currentAccounts; i++) {
			for (unsigned int j = 0; j < MAXCHARACTERS; j++) {
				aPlayer[i].aCharacters[j].charName = aPlayer[i + 1].aCharacters[j].charName;
				aPlayer[i].aCharacters[j].charClass = aPlayer[i + 1].aCharacters[j].charClass;
			}
		}
		for (unsigned int i = currentPlayer; i < STARTINGACCOUNTS; i++) {
			aPlayer[i].name = aPlayer[i + 1].name;
			aPlayer[i].password = aPlayer[i + 1].password;
			aPlayer[i].admin = aPlayer[i + 1].admin;
			aPlayer[i].indexChar = aPlayer[i + 1].indexChar;
		}
		ClearScreen();
		currentAccounts--;
		Login(aPlayer, currentPlayer);
	}
}

void PlayerMenu() {
	ClearScreen();
	/*std::cout << "*****ELIGE UNA OPCI�N*****"<< std::endl;
	std::cout << "1.- Crear Personaje" << std::endl;
	std::cout << "2.- Listar Personajes" << std::endl;
	std::cout << "3.- Eliminar personaje" << std::endl;
	std::cout << "4.- Eliminar cuenta" << std::endl;
	std::cout << "5.- Salir" << std::endl;*/

	PlayerMenuASCII();

}

void AdminMenu() {
	ClearScreen();
	/*std::cout << "*****ELIGE UNA OPCI�N*****" << std::endl;
	std::cout << "1.- Listar cuentas de jugador" << std::endl;
	std::cout << "2.- Eliminar cuenta de jugador" << std::endl;
	std::cout << "3.- Mostrar personajes de un jugador" << std::endl;
	std::cout << "4.- Eliminar personaje de jugador" << std::endl;
	std::cout << "5.- Salir" << std::endl;*/

	AdminMenuASCII();
}

void AccountList(Players* aPlayer) {

	for (unsigned int i = 0; i < STARTINGACCOUNTS; i++) {
		unsigned int count = 0;
		if (aPlayer[i].name != "" && aPlayer[i].admin == false) {
			for (unsigned int j = 0; j < MAXCHARACTERS; j++) {
				if (aPlayer[i].aCharacters[j].charName != "") {
					count++;
				}
			}
			std::cout << aPlayer[i].name << "tiene " << count << " personsjes." << std::endl;
		}
	}
	IntroContinue();
}
void DeleteOther(Players* &aPlayer, unsigned int &currentAccounts, int &currentPlayer) {
	char ans;
	std::string playerAccount;
	int count = 0;
	int playerSelected;
	std::cout << "�Que cuenta de jugador quiere eliminar?" << std::endl;
	std::cin >> playerAccount;
	for (unsigned int i = 0; i < currentAccounts; i++) {
		if (aPlayer[i].name != playerAccount) {
			count++;
		}
		else if (aPlayer[i].name == playerAccount) {
			playerSelected = count;
		}
	}
	if (count == currentAccounts || aPlayer[playerSelected].admin == true) {
		std::cout << "Esta cuenta no existe o es de administrador" << std::endl;
		IntroContinue();
		return;
	}
	std::cout << "�Esta seguro de querer eliminar la cuenta " << playerAccount << "? (S)Si (N)No" << std::endl;
	std::cin >> ans;
	while (ans != 'S' && ans != 'N' && ans != 's' && ans != 'n') {
		std::cout << "\nHas introducido una respuesta err�nea.\n�Esta seguro de querer eliminar la cuenta " << playerAccount << "? (S)Si (N)No" << std::endl;
		std::cin >> ans;
	}
	if (ans == 'S' || ans == 's') {
		for (int i = playerSelected; i < currentAccounts; i++) {
			for (unsigned int j = 0; j < MAXCHARACTERS; j++) {
				aPlayer[i].aCharacters[j].charName = aPlayer[i + 1].aCharacters[j].charName;
				aPlayer[i].aCharacters[j].charClass = aPlayer[i + 1].aCharacters[j].charClass;
			}
		}
		for (unsigned int i = playerSelected; i < currentAccounts; i++) {
			aPlayer[i].name = aPlayer[i + 1].name;
			aPlayer[i].password = aPlayer[i + 1].password;
			aPlayer[i].admin = aPlayer[i + 1].admin;
			aPlayer[i].indexChar = aPlayer[i + 1].indexChar;
		}
		if (playerSelected < currentPlayer) {
			currentPlayer--;
		}
		ClearScreen();
	}
}

void ShowPlayerChar(Players* aPlayer) {

	std::string showClass = "";
	std::string playerAccount;
	int count = 0;
	int playerSelected;
	std::cout << "�Que cuenta de jugador quiere revisar?" << std::endl;
	std::cin >> playerAccount;
	for (unsigned int i = 0; i < currentAccounts; i++) {
		if (aPlayer[i].name != playerAccount) {
			count++;
		}
		else if (aPlayer[i].name == playerAccount) {
			playerSelected = count;
		}
	}
	if (count == currentAccounts || aPlayer[playerSelected].admin == true) {
		std::cout << "Esta cuenta no existe o es de administrador" << std::endl;
		IntroContinue();
		return;
	}
	unsigned int index = aPlayer[playerSelected].indexChar;

	std::cout << "Sus personajes actuales s�n: " << std::endl;
	for (unsigned int i = 0; i < index; i++) {
		AdminShowClass(aPlayer, playerSelected, i, showClass);
		std::cout << aPlayer[playerSelected].aCharacters[i].charName << ", " << showClass << "." << std::endl;
	}
	IntroContinue();
}

void DeleteOtherChar(Players* &aPlayer, unsigned int &currentAccounts) {
	char ans;
	std::string playerAccount;
	std::string playerChar;
	int count = 0;
	int playerSelected;
	int charSelected;
	std::cout << "�De que cuenta de jugador quiere eliminar un personaje?" << std::endl;
	std::cin >> playerAccount;
	for (unsigned int i = 0; i < currentAccounts; i++) {
		if (aPlayer[i].name != playerAccount) {
			count++;
		}
		else if (aPlayer[i].name == playerAccount) {
			playerSelected = count;
		}
	}
	if (count == currentAccounts || aPlayer[playerSelected].admin == true) {
		std::cout << "Esta cuenta no existe o es de administrador" << std::endl;
		IntroContinue();
		return;
	}
	std::cout << "�Que personaje quiere eliminar?" << std::endl;
	std::cin >> playerChar;
	count = 0;
	for (unsigned int i = 0; i < currentAccounts; i++) {
		if (aPlayer[playerSelected].aCharacters[i].charName != playerChar) {
			count++;
		}
		else if (aPlayer[playerSelected].aCharacters[i].charName == playerChar) {
			charSelected = count;
		}
	}
	if (count == aPlayer[playerSelected].indexChar) {
		std::cout << "Este personaje no existe." << std::endl;
		IntroContinue();
		return;
	}
	for (unsigned int i = 0; i < aPlayer[playerSelected].indexChar; i++) {
		if (playerChar == aPlayer[playerSelected].aCharacters[i].charName) {
			std::cout << "�Esta seguro de querer eliminar a " << aPlayer[playerSelected].aCharacters[i].charName << "? (S)Si (N)No" << std::endl;
			std::cin >> ans;
			while (ans != 'S' && ans != 'N' && ans != 's' && ans != 'n') {
				std::cout << "\nHas introducido una respuesta err�nea.\n�Esta seguro de querer eliminar a " << aPlayer[playerSelected].aCharacters[i].charName << " ? (S)Si(N)No" << std::endl;;
				std::cin >> ans;
			}
			if (ans == 'S' || ans == 's') {
				for (unsigned int j = i; j < aPlayer[playerSelected].indexChar; j++) {
					aPlayer[playerSelected].aCharacters[j].charName = aPlayer[playerSelected].aCharacters[j + 1].charName;
					aPlayer[playerSelected].aCharacters[j].charClass = aPlayer[playerSelected].aCharacters[j + 1].charClass;
				}
				aPlayer[playerSelected].indexChar--;
			}
		}
	}

}

//1.D, 2.A, 2.B,
void ChargeMenu(Players* aPlayer, int currentPlayer) {
	unsigned int option;
	if (aPlayer[currentPlayer].admin == true){
		
		AdminMenu();
		std::cin >> option;
		ClearScreen();

		while (option < 1 || option > 5) {
			std::cout << "Has introducido un valor err�neo";
			std::cin >> option;
		}
		switch (option) {
		case 1:
			
			AccountList(aPlayer);
			break;

		case 2: 
			
			DeleteOther(aPlayer, currentAccounts, currentPlayer);
			break;

		case 3:

			ShowPlayerChar(aPlayer);
			break;

		case 4:

			DeleteOtherChar(aPlayer, currentAccounts);
			break;

		case 5: 

			Login(aPlayer, currentPlayer);			
			break;
		}

	}
	else {
		PlayerMenu();
		std::cin >> option;
		ClearScreen();

		while (option < 1 || option > 5) {
			std::cout << "Has introducido un valor err�neo" << std::endl;
			std::cin >> option;
		}
		switch (option) {
		case 1:
			
			CreateChar(aPlayer, currentPlayer);
			break;

		case 2:

			ShowChar(aPlayer, currentPlayer);
			break;

		case 3:

			DeleteChar(aPlayer, currentPlayer);
			break;

		case 4:

			DeleteSelf(aPlayer, currentPlayer, currentAccounts);
			break;

		case 5: 
			Login(aPlayer, currentPlayer);
			break;
		}
	}
}

void InitAccounts(Players* aPlayer) {
	unsigned int i = 0;
	for (i; i < STARTINGPLAYERS; i++) {
		aPlayer[i].name = "Player" + (std::to_string(i));
		aPlayer[i].password = "pass" + (std::to_string(i));
		aPlayer[i].admin = false;
	}
	aPlayer[i].name = "enti";
	aPlayer[i].password = "enti";
	aPlayer[i].admin = true;

}

int main() {
	setlocale(LC_ALL, "");
	AdminMenuASCII();
	IntroContinue();
	Animation();
	IntroContinue();
	ClearScreen();
	InitAccounts(aPlayer);		
	Login(aPlayer, currentPlayer);
	while (gameLoop) {
		ChargeMenu(aPlayer, currentPlayer);
	}
}
