/*

PARA BUSCAR ALGO EN EL DOCUMENTO, CNTRL+F
Los titulos est�n marcados con 5 :::::
Ejemplo: 
::::: CLEAR SCREEN :::::

*/

//TODO: 
/*ANTES DE ENTREGAR PRACTICA DE LA DUNGEON, PASAR A LIMPIO LA REFERENCIA EN UN ARCHIVO .H Y USARLO EN EL PROPIO EJERCICIO*/


/* ::::: CLEAR SCREEN ::::: */

/*

Estaba buscando como limpiar la pantalla, recordaba que en C habia algo como system("CLS").
Leyendo el articulo veo que compromete la seguridad al depender del sistema donde se ejecuta.

Code author: Duthomhas
void ClearScreen(): http://www.cplusplus.com/articles/4z18T05o/#OSSpecificWays
Why system() is evil: http://www.cplusplus.com/articles/j3wTURfi/

*/

#include <windows.h>

void ClearScreen()
{
	HANDLE                     hStdOut;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	DWORD                      count;
	DWORD                      cellCount;
	COORD                      homeCoords = { 0, 0 };

	hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	if (hStdOut == INVALID_HANDLE_VALUE) return;

	/* Get the number of cells in the current buffer */
	if (!GetConsoleScreenBufferInfo(hStdOut, &csbi)) return;
	cellCount = csbi.dwSize.X *csbi.dwSize.Y;

	/* Fill the entire buffer with spaces */
	if (!FillConsoleOutputCharacter(
		hStdOut,
		(TCHAR) ' ',
		cellCount,
		homeCoords,
		&count
	)) return;

	/* Fill the entire buffer with the current colors and attributes */
	if (!FillConsoleOutputAttribute(
		hStdOut,
		csbi.wAttributes,
		cellCount,
		homeCoords,
		&count
	)) return;

	/* Move the cursor home */
	SetConsoleCursorPosition(hStdOut, homeCoords);
}

/* ::::: PULSAR UNA TECLA PARA CONTINUAR + LIMPIA PANTALLA ::::: */
/*�til para mejorar la comunicaci�n con el usuario en escenarios donde:
 - estamos validando errores, tras mostrar el fallo queremos limpiar pantalla e iniciar una nueva iteraci�n
 - es necesario un salto de p�gina (ej: Lectura de un libro)*/
void PulsaContinuar()
{
	std::cout << "\nPulsa cualquier tecla para continuar...";
	std::cin.ignore();
	std::cin.get();
	ClearScreen();
}



/* ::::: INCLUIR TILDES ::::: */

//Librerias necesarias
#include <wchar.h>
#include <locale.h>



void main1()
{
	setlocale(LC_ALL, ""); // De esta manera se printa por pantalla correctamente los char tipo { � � � � }
}

/* ::::: BASICO ::::: */

#include <iostream> //libreria std::

void main2()
{
	// M�s polite que un system(pause). Cuando usuario presiona tecla, el codigo procede.
	std::cin.ignore();
	std::cin.get();

}